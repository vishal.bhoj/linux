# SPDX-License-Identifier: GPL-2.0
lib-y		:= clear_user.o delay.o copy_from_user.o		\
		   copy_to_user.o copy_page.o				\
		   clear_page.o csum.o insn.o memchr.o 			\
		   memset.o memcmp.o strcmp.o strncmp.o strlen.o	\
		   strnlen.o strchr.o strrchr.o tishift.o

ifeq ($(CONFIG_ARM64_MORELLO), y)
lib-y += morello_memcpy.o
else
lib-y += memcpy.o
endif

ifeq ($(CONFIG_ARM64_MORELLO)$(CONFIG_EFI),yy)
stub_targets := memcpy.o
stub_targets_altered := $(stub_targets:%.o=%_notags.o)

targets += $(stub_targets_altered)

# EFI stub dedicated version:
# - dropping exporting symbols
$(foreach __obj,$(stub_targets_altered), \
	$(eval AFLAGS_$(__obj) = -D__DISABLE_EXPORTS))

# Following the approach taken in drivers/firmware/efi/libstub/Makefile:
# placing the symbols in EFI stub namespace and additionally within the
# .init section, as EFI stub is the only intended use of those
OBJCOPYFLAGS := --prefix-symbols=__efistub_ \
		--prefix-alloc-sections=.init

$(obj)/%_notags.o: $(src)/%.S FORCE
	$(call if_changed_rule,as_o_S)

$(obj)/%_notags.stub.o: $(obj)/%_notags.o FORCE
	$(call if_changed,objcopy)

obj-y += $(stub_targets_altered:%.o=%.stub.o)
endif

ifeq ($(CONFIG_KERNEL_MODE_NEON), y)
obj-$(CONFIG_XOR_BLOCKS)	+= xor-neon.o
CFLAGS_REMOVE_xor-neon.o	+= -mgeneral-regs-only
CFLAGS_xor-neon.o		+= -ffreestanding
# Enable <arm_neon.h>
CFLAGS_xor-neon.o		+= -isystem $(shell $(CC) -print-file-name=include)
endif

lib-$(CONFIG_ARCH_HAS_UACCESS_FLUSHCACHE) += uaccess_flushcache.o

obj-$(CONFIG_CRC32) += crc32.o

obj-$(CONFIG_FUNCTION_ERROR_INJECTION) += error-inject.o

obj-$(CONFIG_ARM64_MTE) += mte.o

obj-$(CONFIG_ARM64_MORELLO) += morello.o \
			       copy_from_user_with_captags.o \
			       copy_to_user_with_captags.o

obj-$(CONFIG_KASAN_SW_TAGS) += kasan_sw_tags.o
